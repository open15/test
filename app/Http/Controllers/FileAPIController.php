<?php

namespace App\Http\Controllers;

class FileAPIController extends Controller
{
	/**
     * Display the resource.
     * example: bytes_by_file/apple-logo.jpg
     *
     * @return string
	 */
    public function bytesByFile($fileName)
    {
        $filePath = "upload/" . $fileName;
        $handle = fopen($filePath, "rb");
        $byteArray = [];

        fread($handle, 100);

        for($i = 0; $i < 100; $i++)
        {
            $data = fread($handle, 1);
            $byteArray[] = bin2hex($data);
        }

        fclose($handle);

        return response()->json($byteArray, 200);
    }
}
