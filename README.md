## Tasks
1. PHP

        run bytes_by_file/apple-logo.jpg
   
        Код:

        public function bytesByFile($fileName)
        {
            $filePath = "upload/" . $fileName;
                //Открываем "rb" не текстовый файл
            $handle = fopen($filePath, "rb");
            $byteArray = [];
        
                //Читаем первый 100 байт и не запоминаем
            fread($handle, 100);
        
            for($i = 0; $i < 100; $i++)
            {
                    //По байту читаем 100 байт, иначе нам вернется строка без отдельного байта
                $data = fread($handle, 1);
                    //Переводим байт в шестнадцатеричное представление
                $byteArray[] = bin2hex($data);
            }
            
            fclose($handle);
    
            return response()->json($byteArray, 200);
        }
2. SQL
        
        Найти все записи из “a” которые содержат неверные ссылки на “b”
   
        Left Excluding Join

        select a.*
        from a
        left join b on a.b_id = b.id
        where b.id is null;
3. JS
   
        <div id = "advert"></div>
        <div id = "promo"></div>
        <script>
            let script = document.createElement('script');
            script.src = "https://load_advert.js"
   
            document.head.append(script);
   
            script.onerror = function() {
                document.getElementById("promo").style.display = "block";
            };
        </script>
   
        Создаем элемент script, добавляем наш src, в случае ошибки в работе скрипта элемент с id "promo" станет видимым
4. Linux

        cat my.log | grep -oP '(?<=;).*(?=;\sGET;|;\sPOST|;\sPUT|;\sPATCH|;\sDELETE|;\sHEAD|;\sCONNECT|;\sOPTIONS|;\sTRACE)' | sort | uniq -c

        cat my.log - В файле лога
        grep - Найти
        -oP - Точное совпадение regexp Perl
        uniq -c - Количество уникальных записей

        Regexp: (?<=;).*(?=;\sGET|;\sPOST|;\sPUT|;\sPATCH|;\sDELETE|;\sHEAD|;\sCONNECT|;\sOPTIONS|;\sTRACE)
        
        .* - Любое выражение
        (?<=;) - Перед которым есть символ ';'
        (?=;\sGET) - После которого метод запроса '; GET'
        | - Или любой другой метод запроса

5. Кэш

        Дано:

        $Cache = new Memcache;
        …
        if (!($data = $Cache->get($key))) {
        $data = … hard work …
        $Cache->set($key, $data, …);
        }

        Но лучше:

        $Cache = new Memcache;
        …
        if (!$Cache->exist('my_hard_work_run') && !$Cache->exist($key)) {
        
        $Cache->set('my_hard_work_run', 1, …) 
        $data = … hard work …
        $Cache->forget('my_hard_work_run')
        
        $Cache->set($key, $data, …);
        }

        Неизвестно сколько может отрабатывать скрипт "… hard work …"
        Если придет 5 запросов, то скрипт "… hard work …" может выполнится 5 раз,
        если скрипт еще не отработал в первый раз 
        
        Можно записать в Кэш информацию о том что скрипт "… hard work …" выполняется
        и проверять при попытке повторного запуска

## Installation Project

        1. Clone the project to parent directory
        
                $ git clone https://gitlab.com/open15/test.git
        2. Go to project
        
                $ cd /test
        3. Init project
        
                $ composer i
        4. Run project

                $ php artisan serve
